require('dotenv').config();

const fs = require("fs");

const mysql = require("mysql");

//Creating connection
const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB
});

//Open Connection
connection.connect((err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("connected");

        connection.query(matchesTableQuery, (error, response) => {
            console.log(error || response);
        });

        connection.query(deliveriesTableQuery, (error, response) => {
            console.log(error || response);
        });

    }
});

let matchesTableQuery = `CREATE TABLE IF NOT EXISTS matches(

    id int DEFAULT NULL,

    season int DEFAULT NULL,

    city varchar(200),

    date Date,

    team1 varchar(200),

    team2 varchar(200),

    toss_winner varchar(200),

    toss_decision varchar(200),

    result varchar(200),

    dl_applied int DEFAULT NULL,

    winner varchar(200),

    win_by_runs int DEFAULT NULL,

    win_by_wickets int DEFAULT NULL,

    player_of_match varchar(200),

    venue varchar(200),

    umpire1 varchar(200),

    umpire2 varchar(200),

    umpire3 varchar(200)
)`;


const deliveriesTableQuery = "CREATE TABLE IF NOT EXISTS `deliveries` (\
`match_id` int DEFAULT NULL,\
`inning` int DEFAULT NULL,\
`batting_team` varchar(200),\
`bowling_team` varchar(200),\
`over` int DEFAULT NULL,\
`ball` int DEFAULT NULL,\
`batsman` varchar(200),\
`non_striker` varchar(200),\
`bowler` varchar(200),\
`is_super_over` int DEFAULT NULL, \
`wide_runs` int DEFAULT NULL, \
`bye_runs` int DEFAULT NULL, \
`legbye_runs`int DEFAULT NULL, \
`noball_runs` int DEFAULT NULL, \
`penalty_runs` int DEFAULT NULL, \
`batsman_runs` int DEFAULT NULL, \
`extra_runs` int DEFAULT NULL, \
`total_runs` int DEFAULT NULL, \
`player_dismissed` varchar(200), \
`dismissal_kind` varchar(200), \
`fielder` varchar(200)\
)";




module.exports = connection;