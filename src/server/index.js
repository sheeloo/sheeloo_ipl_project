const fs = require("fs");

const path = require('path')

const database = require('./mysqlConnection');

const output_file_path = path.join(__dirname, "../public/output/");

const iplStats = require("./ipl.js");

//save_JSON_files(iplStats);

function save_JSON_files(Queries) {

    try {

        for (let query in Queries) {

            if (Queries[query] === undefined) { return }

            database.query(Queries[query], function(err, result, fields) {

                if (err) throw err;

                let jsonString = JSON.stringify(result);

                fs.writeFile(`${output_file_path}${query}.json`, jsonString, "utf-8", function(err, jsonString) {
                    if (err) throw err;
                });
            });
        }

    } catch (error) {

        console.log(error);

    }

}