const http = require("http");
const path = require("path");
const fs = require("fs");

const server = http.createServer((request, response) => {

    const reqUrl = request.url;
    const reqUrlFileExt = request.url.split(".").splice(-1)[0];
    const clientFolderPath = path.join(__dirname, "../client/");
    const outputFolderPath = path.join(__dirname, "../public/output");

    switch (reqUrlFileExt) {

        case '/':

            const htmlFilePath = path.join(__dirname, "../client/index.html");

            fs.readFile(htmlFilePath, "utf-8", (err, htmlData) => {
                if (err) {
                    response.writeHead(404);
                } else {
                    response.writeHead(200, { 'Content-Type': "text/html" });
                    response.end(htmlData);
                }
            });
            break;

        case 'css':

            const cssFilePath = path.join(__dirname, "../client/style.css");

            fs.readFile(cssFilePath, "utf-8", (err, cssData) => {
                if (err) {
                    response.writeHead(404);
                    console.log(err)
                } else {
                    response.writeHead(200, { 'Content-Type': "text/css" });
                    response.end(cssData);
                }
            });
            break;

        case 'js':

            const jsFilePath = path.join(__dirname, "../client/plots.js");

            console.log(jsFilePath)
            fs.readFile(jsFilePath, "utf-8", (err, jsData) => {
                if (err) {
                    response.writeHead(404);
                    console.log(err)
                } else {
                    response.writeHead(200, { 'Content-Type': "application/javascript" });
                    response.end(jsData);
                }
            });
            break;


        case 'json':

            const jsonFilePath = path.join(outputFolderPath, reqUrl);

            fs.readFile(jsonFilePath, "utf-8", (err, jsonData) => {
                if (err) {
                    response.writeHead(404);
                } else {
                    response.writeHead(200, { 'Content-Type': "application/json" });
                    response.end(jsonData);
                }
            });
            break;

    }

});

server.listen(3000);