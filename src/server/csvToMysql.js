const fs = require("fs");
const fast_csv = require("fast-csv");
const path = require('path');
const database = require('./mysqlConnection');

async function csvToMysql(fileName, table) {
    try {

        let stream = await fs.createReadStream(path.join(__dirname, `../data/${fileName}`));
        let csvData = [];
        let csvStream = fast_csv
            .parse()
            .on("data", function(data) {
                csvData.push(data);
            })
            .on("end", function() {
                // remove the first line: header

                const categories = csvData[0].slice();

                csvData.shift();

                // connect to the MySQL database
                // save csvData
                const query = `INSERT INTO ${table} (${categories}) VALUES ?`;

                database.query(query, [csvData], (error, response) => {
                    console.log(error || response);
                });

            });



        stream.pipe(csvStream);


    } catch (error) {

        console.log(error);
    }

}

async function saveData() {

    try {

        await csvToMysql("matches.csv", "matches");
        await csvToMysql("deliveries.csv", "deliveries");

    } catch (error) {
        console.log(error);
    }

}

saveData();