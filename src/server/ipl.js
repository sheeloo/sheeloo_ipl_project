const iplStats = {};

/**
 * Number of matches played per year for all the years in IPL.
 */
iplStats.matchesPlayedPerYear = "select season, count( * ) as played_matches FROM matches GROUP BY season ";


/**
 * Number of matches won per team per year in IPL.
 */

iplStats.matchesWonPerTeamPerYear = "select season, winner, count( * ) as won FROM matches GROUP BY season, winner";


/**
 * Extra runs conceded per team in the year 2016
 */

iplStats.extraRunsConcededPerTeamIn2016 = "select bowling_team, sum(extra_runs) as Extra_runs_conceded from deliveries inner join matches on matches.id = deliveries.match_id where matches.season = 2016 group by bowling_team ";


/**
 * Top 10 economical bowlers in the year 2015.
 */
iplStats.top10EconomicalBowlersIn2015 = "select bowler as Bowler, round(sum(total_runs) / (count(bowler) / 6), 2) as Economy_rate from deliveries \
inner join matches on matches.id = deliveries.match_id where matches.season = 2015 \
group by bowler order by Economy_rate asc limit 10 ";


/**
 */
iplStats.eachTeamWonTossAndMatch = "select winner, count( * ) as won FROM matches where winner = toss_winner GROUP BY winner";


/**
 * Find a player who has won the highest number of Player of the Match awards for each season.
 */
iplStats.playerwonHighestNumberofPlayeroftheMatch = "select season, player_of_match, count(player_of_match) as won from matches \
group by season, player_of_match order by season asc, won desc";


/**
 * Find the strike rate of a batsman for each season.
 */
iplStats.strikeRateOfBatsmanEachSeason = "select season, batsman as Batsman, round((sum(batsman_runs) / count(ball)) * 100, 2) as Strike_rate from deliveries \
inner join matches on matches.id = deliveries.match_id \
group by season, batsman ";


/**
 * Find the highest number of times one player has been dismissed by another player.
 */
iplStats.HighestTimesPlayerDismissedbyAnotherPlayer = "select player_dismissed, count( * ) as dismiss from deliveries \
where dismissal_kind != '' and dismissal_kind != 'retired hurt' and dismissal_kind != 'obstructing the field' \
group by player_dismissed order by dismiss desc limit 1 ";


/**
 * Find the bowler with the best economy in super overs.
 */
iplStats.bestEconomicalBowlerInSuperOvers = "select bowler as Bowler, round(sum(total_runs) / count(bowler) / 6, 2) as Economy_rate from deliveries \
inner join matches on matches.id = deliveries.match_id where deliveries.is_super_over = 1 \
group by bowler order by Economy_rate asc limit 1 ";



module.exports = iplStats;