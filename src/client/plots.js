function visualizeMatchesPlayedPerYear() {

    try {

        fetch("./matchesPlayedPerYear.json")
            .then(r => r.json())
            .then(matchesPlayedPerYear => {

                const seriesData = matchesPlayedPerYear.map(obj => Object.values(obj));;

                Highcharts.chart("matchesPlayedPerYear", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Matches Played Per Year"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Matches"
                        }
                    },
                    series: [{
                        name: "Years",
                        data: seriesData
                    }]
                });
            });

    } catch (error) {
        console.log(error);
    }

}

visualizeMatchesPlayedPerYear();
visualizeExtraRunsConcededByEachTeamIn2016();
visualizeTenEconomicalBowlersOf2015();
visualizeMatchesWonByEachTeam();

function visualizeMatchesWonByEachTeam() {

    try {

        fetch("./matchesWonPerTeamPerYear.json")
            .then(r => r.json())
            .then(matchesWonByEachTeam => {

                const seasons = [...new Set(matchesWonByEachTeam.map(a => a.season))].sort((a, b) => a - b);

                const teams = [...new Set(matchesWonByEachTeam.map(a => a.winner))]

                let seriesData = [];
                seriesData = teams.map(team => ({
                    name: team,
                    data: matchesWonByEachTeam.filter(a => a.winner === team).map(a => a.won)
                }));

                Highcharts.chart("matchesWonByEachTeam", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Matches won by each team over all the years of IPL"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        title: {
                            text: "Teams"
                        },
                        categories: seasons,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Wins'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: seriesData
                });

            });

    } catch (error) {
        console.log(error);
    }


}


function visualizeExtraRunsConcededByEachTeamIn2016() {

    try {

        fetch("./extraRunsConcededPerTeamIn2016.json")
            .then(r => r.json())
            .then(extraRunsConcededByEachTeamIn2016 => {

                const seriesData = extraRunsConcededByEachTeamIn2016.map(obj => Object.values(obj));

                Highcharts.chart("extraRunsConcededByEachTeamIn2016", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "For the year 2016, extra runs conceded by each team"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },

                    yAxis: {
                        min: 0,
                        title: {
                            text: "Extra Runs"
                        }
                    },
                    series: [{
                        name: "Teams",
                        data: seriesData
                    }]
                });

            });

    } catch (error) {
        console.log(error);
    }


}

function visualizeTenEconomicalBowlersOf2015() {

    try {
        fetch("./top10EconomicalBowlersIn2015.json")
            .then(r => r.json())
            .then(tenEconomicalBowlersOf2015 => {

                let seriesData = tenEconomicalBowlersOf2015.map(obj => Object.values(obj));;

                Highcharts.chart("tenEconomicalBowlersOf2015", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "For the year 2015, top 10 economical bowlers along with their economy rates"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Economy Rate"
                        }
                    },
                    series: [{
                        name: "Bowlers",
                        data: seriesData
                    }]
                });

            });

    } catch (error) {
        console.log(error);
    }


}


visualizeEachTeamWonTossAndMatch();
visualizePlayerwonHighestNumberofPlayeroftheMatch();
createDropDown();
visualizeHighestTimesPlayerDismissedbyAnotherPlayer();
visualizebestEconomicalBowlerInSuperOvers();

function visualizeEachTeamWonTossAndMatch() {

    try {

        fetch("./eachTeamWonTossAndMatch.json")
            .then(r => r.json())
            .then(eachTeamWonTossAndMatch => {

                const seriesData = eachTeamWonTossAndMatch.map(obj => Object.values(obj));

                const teams = eachTeamWonTossAndMatch.map(a => a.winner);

                Highcharts.chart("eachTeamWonTossAndMatch", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Teams won toss and match"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },

                    xAxis: {
                        categories: teams
                    },

                    yAxis: {
                        min: 0,
                        title: {
                            text: "Matches"
                        }
                    },

                    series: [{
                        name: "Teams",
                        data: seriesData
                    }]
                });

            });
    } catch (error) {
        console.log(error);
    }

}


function visualizePlayerwonHighestNumberofPlayeroftheMatch() {

    try {

        fetch("./playerwonHighestNumberofPlayeroftheMatch.json")
            .then(r => r.json())
            .then(playerwonHighestNumberofPlayeroftheMatch => {

                const season = [...new Set(playerwonHighestNumberofPlayeroftheMatch.map(a => a.season))].sort((a, b) => a - b);

                const playersBySeason = season.map(a => playerwonHighestNumberofPlayeroftheMatch.find(e => e.season == a));
                const seriesData = playersBySeason.map(obj => [obj.player_of_match, obj.won]);

                Highcharts.chart("playerwonHighestNumberofPlayeroftheMatch", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Player won Highest Number of times Player of the Match"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        categories: season
                    },

                    yAxis: {
                        min: 0,
                        title: {
                            text: "Highest Times"
                        }
                    },
                    series: [{
                        name: "Player of the Match",
                        data: seriesData
                    }]
                });
            });

    } catch (error) {
        console.log(error);
    }
}



function createDropDown() {

    try {

        fetch("./strikeRateOfBatsmanEachSeason.json")
            .then(r => r.json())
            .then(strikeRateOfBatsmanEachSeason => {

                var x = document.getElementById("seasons");

                const seasons = [...new Set(strikeRateOfBatsmanEachSeason.map(a => a.season))].sort((a, b) => a - b);

                seasons.forEach(season => {
                    var option = document.createElement("option");
                    option.text = season;
                    option.value = season;
                    x.add(option);
                });
            });

    } catch (error) {
        console.log(error);
    }

}

function visualizeStrikeRateOfBatsmanEachSeason() {

    try {

        fetch("./strikeRateOfBatsmanEachSeason.json")
            .then(r => r.json())
            .then(strikeRateOfBatsmanEachSeason => {

                var seasonsDropDown = document.getElementById("seasons");
                var season = seasonsDropDown.options[seasonsDropDown.selectedIndex].value;

                if (season === "") { return }

                const seriesData = strikeRateOfBatsmanEachSeason.filter(a => a.season == season).map(obj => [obj.Batsman, obj.Strike_rate]);

                Highcharts.chart("strikeRateOfBatsmanEachSeason", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Strike Rate of Batsman for Season " + season
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Strike Rate"
                        }
                    },
                    series: [{
                        name: "Batsman",
                        data: seriesData
                    }]
                });


            });

    } catch (error) {
        console.log(error);
    }

}

function visualizeHighestTimesPlayerDismissedbyAnotherPlayer() {

    try {

        fetch("./HighestTimesPlayerDismissedbyAnotherPlayer.json")
            .then(r => r.json())
            .then(HighestTimesPlayerDismissedbyAnotherPlayer => {

                const seriesData = HighestTimesPlayerDismissedbyAnotherPlayer.map(obj => Object.values(obj));

                Highcharts.chart("HighestTimesPlayerDismissedbyAnotherPlayer", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Highest Times Player Dismissed by Another Player"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Dismissal Times"
                        }
                    },
                    series: [{
                        name: "Player",
                        data: seriesData
                    }]
                });
            });

    } catch (error) {
        console.log(error);
    }

}

function visualizebestEconomicalBowlerInSuperOvers() {

    try {

        fetch("./bestEconomicalBowlerInSuperOvers.json")
            .then(r => r.json())
            .then(bestEconomicalBowlerInSuperOvers => {

                const seriesData = bestEconomicalBowlerInSuperOvers.map(obj => Object.values(obj));

                Highcharts.chart("bestEconomicalBowlerInSuperOvers", {
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Best Economical Bowler in Super Overs"
                    },
                    subtitle: {
                        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
                    },
                    xAxis: {
                        type: "category"
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Economy Rate"
                        }
                    },
                    series: [{
                        name: "Bowler",
                        data: seriesData
                    }]
                });
            });

    } catch (error) {
        console.log(error);
    }

}